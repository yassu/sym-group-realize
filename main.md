---
marp: true
theme: default
paginate: true
---

# 対称群の中に任意の有限群が実現できることの証明

---

目次

1. 主定理を眺める
1. 基本的な記号
1. 写像, 単射, 全射, 全単射
1. 対称群
1. 群の定義と例
1. 準同型/同型写像
1. 部分群
1. 正規部分群
1. 商群
1. 準同型定理
1. 主定理
1. 主定理の証明

---

## 主定理を眺める

位数が$n$の任意の群$G$は, 対称群$S_n$のある部分群と同型である.

---

## 基本的な記号

$\mathbb{N}$: 自然数全体の集合
$\mathbb{Z}$: 整数全体の集合
$\mathbb{Q}$: 有理数全体の集合
$\mathbb{R}$: 実数全体の集合

---

## 写像, 単射, 全単射

二つの集合$X$, $Y$に対して, 全ての$X$の元$x \in X$に対してただ一つの$Y$の元を対応させる規則

$$
\begin{align*}
  f: X \to Y \\
  x \mapsto f(x)
\end{align*}
$$

を写像と呼ぶ.

---

## 写像の例

**例1**

実数$x$に対して, $x^3$を対応させる対応

$$
f: \mathbb{R} \to \mathbb{R}; f(x)=x^3
$$

は写像である.

**例2**

正の実数$y$に対して, 方程式$x^2=y$の解を対応させる規則

$$
f: \{x \in \mathbb{R}; x > 0 \} \to \mathbb{R}; f(x)=\pm \sqrt{x}
$$

は写像ではない.

---

## 単射

集合$X, Y$の間の写像$f \colon X \to Y$が条件

$$
f(x) = f(y) \Rightarrow x=y
$$

を満たすとき, 写像$f$は単射であるという.
標語的には「行き先が同じならば, 元も同じ」.

**例1**

$$
f: \mathbb{R} \to \mathbb{R}; f(x) = x^3
$$

とおく.
$f(x) = f(y)$ 即ち $x^3 = y^3$とすると$x=y$なので, $f$は単射である.

---

**例2**

$$
f: \mathbb{R} \to \mathbb{R}; f(x) = x^2
$$

$f(1) = f(-1)$なので, $f$は単射でない.

---

## 全射

集合$X, Y$の間の写像$f\colon X \to Y$が条件

> $Y$の任意の元$y$に対して, ある$x \in X$があって $f(x)=y$が成り立つ.

を満たすとき, 写像$f$が全射であるという.

**例1**

$$
f\colon \mathbb{Z} \to \mathbb{Z}; f(x) = x + 5
$$

とおく.
任意の整数$y$に対して, $f(y - 5) = y$なので, $f$は全射である.

---

**例2**

$$
f\colon \mathbb{Z} \to \mathbb{R}; f(x) = x
$$

とおく.

$f(x) = \sqrt{2}$を満たす整数$x$は存在しないので, $f$は全射ではない.

---

## 全単射

**定義**: 集合$X, Y$の間の写像$f\colon X \to Y$が全射かつ単射であるとき, $f$は全単射であるという.

**定義** 全単射$f$及び$y \in Y$に対して, $f(x) = y$なる$x$は唯1つに定まる.
このとき, 写像$f^{-1}\colon Y \to X; y \mapsto x$を$f$の逆写像と呼ぶ.

**例1**

$$
f\colon \mathbb{Z} \to \mathbb{Z}; f(x) = x + 5
$$

とおく.
任意の整数$y$に対して, $f(y - 5) = y$なので, $f$は全射である.

また, $f(x) = f(y)$とおくと, $x+5 = y+5$なので, $x=y$である. よって, $f$は単射である.

以上によって, $f$は全単射である.

---

## 対称群

自然数$n$を固定する.
集合$\{1, 2, \ldots, n\}$上の全単射全体の集合を$S_n$とおく.
$S_n$の元を置換と呼ぶ.
$S_n$を対称群と呼ぶ. $S_n$が実際に群であることは後で見る.
$S_n$上に演算を$\sigma, \tau \in S_n$に対して

$$
  \sigma \tau := \sigma \circ \tau
$$

で定義する.

---

## 対称群の記法

$\sigma \in S_n$が

$$
\begin{align*}
  \sigma(i_1) &= j_1 \\
  \sigma(i_2) &= j_2 \\
  &\cdots \\
  \sigma(i_k) &= j_k \\
\end{align*}
$$

であり, $\sigma$が$i$以外のものを固定するものを

$$
\sigma = \begin{pmatrix}
i_1 & i_2 & \cdots & i_k \\
j_1 & j_2 & \cdots & j_k
\end{pmatrix}
$$

と表す.

---

## 対称群の記法2

$S_n$の元である以下のような形の置換を巡回置換と呼ぶ:

$$
\sigma = \begin{pmatrix}
i_1 & i_2 & \cdots & i_k \\
i_2 & i_3 & \cdots & i_1
\end{pmatrix}.
$$

この時, $\sigma = (i_1 i_2 \ldots i_k)$ と書く.

更に, $k=2$であるような巡回置換を互換と呼ぶ.

---

## 群の定義と例

集合$G$上の積と呼ばれる演算

$$
\cdot: G \times G \to G; (x, y) \mapsto x \cdot y
$$

が条件

1. 任意の$a, b, c \in G$に対して $a(bc) = (ab)c$が成り立つ.
2. 単位元と呼ばれる$1_G \in G$があって, 任意の$a \in G$に対して $a 1_G = 1_G a = a$が成り立つ.
3. 任意の$a \in G$に対して$b \in G$があって $ab = ba = 1_G$が成り立つ.

を満たす時, 集合$G$と演算の組$(G, \cdot)$を群と呼ぶ.

簡単のため, 群$G$などと書くこともある.

群$G$の要素の数のことを位数とも呼ぶ.

---

**例1**

$(\mathbb{R}, +), (\mathbb{Q}, +), (\mathbb{Z}, +)$は群である.

---

**例2**

集合$X$上の全単射全体の集合$B(X)$を考える.
$B(X)$は合成に関して群を成す.

1. $f \circ (g \circ h) = (f \circ g) \circ h$は自明.
2. $X$上の恒等写像

$$
  \mathrm{id}_X\colon X \to X; x \mapsto x
$$

に対して $\mathrm{id}_X \circ f = f \circ \mathrm{id}_X = f$である.

3. $f \in B(X)$に対して $f^{-1} \circ f = f \circ f^{-1} = \mathrm{id}_X$.

---

## 準同型/同型写像

$G$, $G^\prime$を二つの群とする.
写像$f\colon G \to G^\prime$が条件

$$
f(ab) = f(a)f(b)
$$

を満たすとき, $f$を準同型写像と呼ぶ.

さらに$f$が全単射であったとき, $f$を同型写像と呼ぶ.

二つの群$G, G^\prime$に対して, 同型写像$f\colon G \to G^\prime$が存在する時, $G$と$G^\prime$が同型であるという.

---

**例** $\mathbb{R}_{>0}:=\{x \in \mathbb{R}; x > 0 \}$は演算を積として群を成す.
一方, $(\mathbb{R}, +)$は群である.

写像
$$
f\colon \mathbb{R}_{>0} \to \mathbb{R}; x \mapsto \log(x)
$$
を考える.

この$f$は同型写像である.
したがって, $\mathbb{R}_{>0}$と$\mathbb{R}$は同型である.

---

## 主張

**主張** 準同型写像$f\colon G_1 \to G_2$が単射 $\Leftrightarrow \mathrm{Ker}(f):=\{x \in G_1; f(x)=e_{G_2} \}$.

**証明** $\Rightarrow$は自明
$\Leftarrow$) $f(x_1) = f(x_2)$とすると, $f(x_1x_2^{-1}) = e_{G_2}$である.
仮定より $x_1x_2^{-1} = e_{G_1}$なので, $x_1=x_2$である.

---

## 主張

$G_1, G_2$を群とし, $f\colon G_1 \to G_2$を準同型写像とする.
この時, $\mathrm{Im}(f) =\{f(g); g \in G_1\} \subset G_2$は$G_2$の部分群である.

**証明**

1. $f(e_{G_1}) = e_{G_2} \in G_2$
1. $f(a)f(b)=f(ab) \in \mathrm{Im}(f)$
1. $f(a^{-1}) = f(a)^{-1} \in \mathrm{Im}(f)$

よって, $\mathrm{Im}(f)$は群を成す.

---

## 部分群

群 $(G, \cdot)$の部分集合$H$が$G$と同じ演算で群になるとき, 即ち$H$が空ではない$G$の部分集合であって

$$
  a, b \in H にたいして ab^{-1} \in H
$$

が成り立つ時, $(H, \cdot)$を群$G$の部分群と呼ぶ.

---

## 正規部分群

群$(G, \cdot)$の部分群$(H, \cdot)$が条件

> 全ての$g \in G$ に対して
> $$ gH = Hg $$

を満たすとき、$H$を$G$の正規部分群と呼ぶ.

---

**例1**

群$G$が全ての$a, b \in G$に対して

$$
ab = ba
$$

を満たす時, 群$G$を可換群と呼ぶ.

可換群は当然 正規部分群である.

---

## 主張

$G_1, G_2$を群として, $f\colon G_1 \to G_2$を準同型とする. この時

$$
  \mathrm{Ker}(f) = \{ g \in G_1; f(g) = e_{G_2} \}
$$

は$G_1$の正規部分群を成す.

**証明**

1. $f(e_{G_1}) = e_{G_2}$より$e_{G_1} \in \mathrm{Ker}(f)$である.
1. $g \in \mathrm{Ker}(f)$に対して$f(g^{-1}) = f(g)^{-1} = 0$ より $g^{-1} \in \mathrm{Ker}(f)$
1. $g_1, g_2 \in \mathrm{Ker}(f)$に対して$f(g_1g_2) = f(g_1)f(g_2) = 0$なので, $g_1g_2 \in \mathrm{Ker}(f)$

なので, $\mathrm{Ker}(f)$は群を成す.

---

さらに, 全ての$g \in G_1, h \in \mathrm{Ker}(f)$に対して

$$
\begin{align*}
f(ghg^{-1}) 
  &= f(g)f(h)f(g^{-1}) \\
  &= f(g)f(g^{-1}) \\
  &= e_{G_2}
\end{align*}
$$

なので, $g\mathrm{Ker}(f)g^{-1} \subset \mathrm{Ker}(f)$である. よって, $g\mathrm{Ker}(f)g^{-1} \subset \mathrm{Ker}(f)$である.
よって, $g\mathrm{Ker}(f) \subset \mathrm{Ker}(f)g$である.
同様にして$\mathrm{Ker}(f)g \subset g\mathrm{Ker}(f)$が成り立つ.
よって, $\mathrm{Ker}(f)g = g\mathrm{Ker}(f)$である.

以上によって, $\mathrm{Ker}(f)$は正規部分群である.

---

## 商群

$G$を群, $N \subset G$を正規部分群とする.
この時, $G/N := \{gN; g \in G \}$は演算

$$
  (g_1N)(g_2N) = g_1g_2N
$$

によって群をなす.
この時の$G/H$を商群と呼ぶ.

---

前の演算がwell-definedであることを示す.

$$
\begin{align*}
g_1N &= g_1^\prime N, \\
g_2N &= g_2^\prime N \\
\end{align*}
$$

のとき,
$$
g_1g_2N = g_1^\prime g_2^\prime N \\
$$

であることを示したい.

実際, $n_a, n_b \in N$が存在して$g_1n_a = g_1^\prime n_b$が成り立つ.
$n_1 = n_an_b^{-1} \in N$とおくと, $g_1^\prime = g_1n_1$となる.
同様に$g_2^\prime = g_2n_2$なる$n_2 \in N$が存在する.

$$
\begin{align*}
  g_1^\prime g_2^\prime
    &= g_1n_1g_2n_2 \\
    &=g_1g_2g_2^{-1}n_1g_2n_2 \\
    &= g_1g_2(g_2n_1g_2^{-1})n_2
\end{align*}
$$

$N$が正規部分群なので, $g_1^\prime g_2^\prime \in g_1g_2N$である. よって, $g_1^\prime g_2^\prime N \subset g_1g_2N$である.
同様に $g_1g_2N \subset g_1^\prime g_2^\prime N$ である. よって $g_1g_2N = g_1^\prime g_2^\prime N$ である.

---

## 準同型定理

$G_1, G_2$を群とする.

$f\colon G_1 \to G_2$を準同型写像とする.
このとき, $G_1 / \mathrm{Ker}(f)$と$\mathrm{Im}(f)$は同型である.

**証明**

$N = G_1 / \mathrm{Ker}(f)$とおく.

写像$\varphi\colon N \to \mathrm{Im}(f)$を
$$
  \varphi(a\mathrm{Ker}(f)) = f(a)
$$
で定義する.

$\varphi$が同型写像であることを示す.

---

1. $\varphi$が写像として定まること.

$aN = bN$ならば $\varphi(aN) = \varphi(bN)$なることを示したい.
$aN=bN$は$a^{-1}b N = N$と同値であるが, これはまた$a^{-1}b \in N$と同値である.

このとき, $f(a^{-1}b) = e_{G_2}$なので, $f(a) = f(b)$である.

2. $\varphi$が同型写像であること

$$
\begin{align*}
\varphi(aN) \varphi(bN)
  &= f(a) f(b) \\
  &= f(ab) \\
  &= \varphi(abN)
\end{align*}
$$

なので, $\varphi$は同型写像である.

---

3. $\varphi$が全単射であること

3-1. $\varphi$が全射であること.

$f(x) \in \mathrm{Im}(f)$に対して, $\varphi(xN) = f(x)$であるので, $\varphi$は全射.

3-2. $\varphi$が単射であること

$\varphi(xN) = \varphi(yN)$とすると, $f(x) = f(y)$なので, $f(x^{-1}y) = f(x)^{-1}f(y) = e_{G_2}$である.
よって, $x^{-1}y \in N$なので, $x^{-1}yN \subset N$であり, $yN \subset xN$である.
同様に$xN \subset yN$なので, $xN = yN$である.

---

**例** 準同型写像$f\colon \mathbb{R} \mapsto S^1$を

$$
  f(\theta) = e^{2\pi i\theta}
$$

で定める.
準同型定理より

$$
  \mathbb{R} / \mathbb{Z} \sim S^1
$$

が分かる.

---
## 主定理

位数が$n$の任意の群$G$は, 対称群$S_n$のある部分群と同型である.

---

**証明**

$G=\{g_1, g_2, \ldots, g_n \}$と置く.
$\rho \in S_n$を

$$
  g_kg_j = g_{\rho_k(j)}
$$

で定義する.
さらに $f\colon G \to S_n; g_k \mapsto \rho_k$ を $f(g_k) = \rho_k$で定める.

1. $eg_j = g_j$より $f(e)=\mathrm{id}$である.
1. $g_k, g_l, g_j \in G$に対して $g_lg_kg_j = g_lg_{\rho_k(j)} = g_{\rho_l\rho_k(j)}$より $f(g_lg_k) = \rho_l\rho_k = f(g_l)f(g_k)$となる.
1. 上より$f(g_k)f(g_k)^{-1} = f(g_k^{-1})f(g_k) = \mathrm{id}$となる.

以上によって, $f$は準同型写像である.

---

また, $f(g) = \mathrm{id} \in S_n$とすると, $g_j = g_{\rho(j)}$となるので, $\rho=\mathrm{id}$より $g=e$となる.
よって, $f$は単射でもある.
以上によって, $f$は単射準同型である.

よって, 準同型定理より$G/\{e\} \sim G$は$\mathrm{Im}(f) \subset S_n$に同型である.

---

## 参考

1. [【置換群】対称群・交代群の定義と性質](https://mathlandscape.com/sym-group)
1. [群の準同型定理](http://www.math.tohoku.ac.jp/~kuroki/LaTeX/20080514_homorphism_theorem.pdf)
1. [準同型定理(第一同型定理)](https://shakayami-math.hatenablog.com/entry/2019/10/12/164519#%E6%BA%96%E5%90%8C%E5%9E%8B%E5%AE%9A%E7%90%86%E7%AC%AC%E4%B8%80%E5%90%8C%E5%9E%8B%E5%AE%9A%E7%90%86)
